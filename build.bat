echo on

pushd "C:\Program Files (x86)\Microsoft Visual Studio\2019\BuildTools\VC\Auxiliary\Build"
call vcvarsall.bat %ARCH% -vcvars_ver=%VCVARS_VER%
popd

echo on

set PYTHONPATH=C:/Python39_%ARCH%
set PATH=%PYTHONPATH:/=\%;%PATH%

pushd cppTango
git apply ..\cmake.patch || goto :error
popd

curl -L https://gitlab.com/api/v4/projects/35357105/packages/generic/libomniorb/%OMNI_VER%/libomniorb-%OMNI_VER%_%ARCH%_%VC_VER%.zip -o libomniorb.zip && ^
curl -L https://gitlab.com/api/v4/projects/35357105/packages/generic/pthreads-win32/%PTHREAD_VER%/pthreads-win32-%PTHREAD_VER%_%ARCH%_%VC_VER%.zip -o pthreads.zip && ^
curl -L https://gitlab.com/api/v4/projects/35357105/packages/generic/libzmq/%ZMQ_VER%/libzmq-%ZMQ_VER%_%ARCH%_%VC_VER%.zip -o libzmq.zip && ^
7z x libomniorb.zip -olibomniorb && ^
7z x pthreads.zip -opthreads && ^
7z x libzmq.zip -olibzmq || goto :error

md idl_build
pushd idl_build

cmake -DCMAKE_INSTALL_PREFIX=../idl ../tango-idl && ^
cmake --build . --config Release --target install || goto :error
popd

set BASE=%CD:\=/%

md deploy

call :compile Release
call :compile Debug

goto :EOF

:compile
rd /s/q %~1 dist tmp
md %~1
pushd %~1

cmake -DCMAKE_BUILD_TYPE=%~1 -DCMAKE_INSTALL_PREFIX=%BASE%/dist -DIDL_BASE=%BASE%/idl ^
      -DOMNI_BASE=%BASE%/libomniorb -DZMQ_BASE=%BASE%/libzmq -DCPPZMQ_BASE=%BASE%/libzmq ^
      -DPTHREAD_WIN=%BASE%/pthreads -DCPACK_PACKAGE_DIRECTORY=%BASE%/tmp ^
      -DBUILD_TESTING=OFF -G"Ninja" %BASE%/cppTango && ^
ninja || goto :error

cpack -C %~1 -V && ^
cpack -C %~1 -V -G ZIP || goto :error
move ..\tmp\*zip ..\deploy
move ..\tmp\*exe ..\deploy

popd
exit /b 0

:error
echo Failed with error #%errorlevel%.
exit /b %errorlevel%
